﻿using PostSharp.Aspects;
using PostSharp.Serialization;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ID.SourceLinkSample.N1
{
    [PSerializable]
    public class NonSenseAspect : OnMethodBoundaryAspect
    {
        protected string Namespace { get; set; }

        protected string ClassName { get; set; }

        protected string MethodName { get; set; }

        protected string MethodFullname { get; set; }


        public override void CompileTimeInitialize(MethodBase method, AspectInfo aspectInfo)
        {
            Namespace = method.ReflectedType.Namespace;
            ClassName = method.ReflectedType.Name;
            MethodName = method.Name;
            MethodFullname = $"{Namespace}.{ClassName}::{MethodName}";

            base.CompileTimeInitialize(method, aspectInfo);
        }

        public override void OnEntry(MethodExecutionArgs args)
        {
            Console.WriteLine($"Entering {MethodFullname}");
        }


    }
}
